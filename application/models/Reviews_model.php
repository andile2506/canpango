<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reviews_model extends CI_Model {

    public function __construct() {
        parent::__construct();  
        //load database library
        $this->load->database();
    }

    /* Fetch review data */
    function getRows($id = ""){
        if(!empty($id)){
            $query = $this->db->get_where('reviews', array('beerid' => $id));
            return $query->result_array();
        }else{
            $query = $this->db->get('reviews');
            return $query->result_array();
        }
    }
    
    /* Insert review data */
    public function insert($data, $id){
        //check if beer with id is in the db yet
        $q = $this->db->get_where('beers', array('id' => $id));
        if($q->num_rows() > 0){
            $insert = $this->db->insert('reviews', $data);
            if($insert){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

}