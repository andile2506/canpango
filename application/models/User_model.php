<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();  
        //load database library
        $this->load->database();
    }

    /* Fetch user data */
    function getRows($id = ""){
        if(!empty($id)){
            $query = $this->db->get_where('user', array('id' => $id));
            return $query->row_array();
        }else{
            $query = $this->db->get('user');
            return $query->result_array();
        }
    }
    
    /* Insert user data */
    public function insert($data = array() ){
        $insert = $this->db->insert('user', $data);
        if($insert)
            return true;
        else
            return false;
    }

}