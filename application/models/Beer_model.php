<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Beer_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        
        //load database library
        $this->load->database();
    }

    /*
     * Fetch user data
     */
    function getRows($id = ""){
        if(!empty($id)){
            $query = $this->db->get_where('beers', array('id' => $id));
            return $query->row_array();
        }else{
            $query = $this->db->get('beers');
            return $query->result_array();
        }
    }
    
    /*
     * Insert user data
     */
    public function insert($data = array()) {
        $insert = $this->db->insert('beers', $data);
        if($insert){
            return true;
        }else{
            return false;
        }
    }
    
    /*
     * Update user data
     */
    public function update($data, $id) {
        if(!empty($data) && !empty($id)){
            $update = $this->db->update('beers', $data, array('id'=>$id));
            return $update?true:false;
        }else{
            return false;
        }
    }
    
    /*
     * Delete user data
     */
    public function delete($id){
        $delete = $this->db->delete('beers',array('id'=>$id));
        return $delete?true:false;
    }

    public function date_last_added(){
        $this->db->select('created_on');
        $this->db->from('beers');
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row();
        if(isset($row)){
            return $row->created_on;
        }
        else{
            return false; 
        }
    }

}