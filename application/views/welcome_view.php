<?php $this->load->view('front/includes/header'); ?>
	<div class="container text-center" id="container">
		<h2>Welcome to Canpango API made with Codeigniter</h2>
		<div class="front-img">
			<img class="img-responsive" src="assets/images/canp.png" />
		</div>
		<a href="<?php echo site_url();?>documentation">Read documentation</a>
	</div>
<?php $this->load->view('front/includes/footer'); ?>