<?php $this->load->view('front/includes/header'); ?>

<div class="container">
	<div class="header-marg-top">
		<div class="row">
			<div class="col-sm-8 offset-sm-2 text-center">
				<h3>Add Beer</h3>
				<?php echo form_open('api/beers/create'); ?>
					<div class="form-group row">
					    <label for="exampleInputEmail1" class="col-sm-2">Beer name</label>
					    <div class="col-sm-10">
					    	<input type="text" class="form-control" id="beername" name="beername" placeholder="Enter Beer Name">
						</div>
					</div>
					<div class="form-group row">
					    <label for="exampleInputEmail1" class="col-sm-2">Beer Ibu</label>
					    <div class="col-sm-10">
					    	<input type="text" class="form-control" id="beeribu" name="beeribu" placeholder="Enter Beer Ibu">
					    </div>
					</div>
					
					<div class="form-group row">
					    <label for="exampleInputEmail1" class="col-sm-2">Beer Calories</label>
					    <div class="col-sm-10">
					    	<input type="text" class="form-control" id="beercalories" name="beercalories" placeholder="Enter Beer Calories">
						</div>
					</div>
					<div class="form-group row">
					    <label for="exampleInputEmail1" class="col-sm-2">Beer Abv</label>
						<div class="col-sm-10">
					    	<input type="text" class="form-control" id="beerabv" name="beerabv" placeholder="Enter Beer Abv">
					    </div>
					</div>
					<div class="form-group row">
					    <label for="exampleInputEmail1" class="col-sm-2">Beer Style</label>
					    <div class="col-sm-10">	
					    	<input type="text" class="form-control" id="beerstyle" name="beerstyle" placeholder="Enter Beer Style">
					    </div>
					</div>

					<div class="form-group row">
					    <label for="exampleInputEmail1" class="col-sm-2">Brewery location</label>
					   	<div class="col-sm-10"> 	
					   	 	<input type="text" class="form-control" id="brewery_location" name="brewery_location" placeholder="Enter Brewery Location">
					   	</div>
					</div>
					<div class="form-group row">
					    <label for="exampleInputEmail1" class="col-sm-2">Category</label>
					    <div class="col-sm-10">
					    	<input type="text" class="form-control" id="category" name="category" placeholder="Enter Category">
					    </div>
					</div>
					<div class="form-group row">
	  					<button type="submit" id="sbmt-btn" class="sbmt-btn btn btn-success btn-lg btn-block">Submit</button>
	  				</div>

	  				<?php if(!empty(validation_errors() ) ){?>
	  					<div class="form-group">
	  						<div class="alert alert-danger" role="alert">
								<?php echo validation_errors(); ?>
							</div>
						</div>
					<?php } ?>
				</form>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('front/includes/footer'); ?>