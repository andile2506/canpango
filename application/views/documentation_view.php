<?php $this->load->view('front/includes/header'); ?>
	<div class="container" id="container">
		<h2 class="text-center">Welcome to <span id="start-img"><img src="assets/images/canp.png"/></span> API</h2>
		<div class="row">
			<nav class="col-md-2 d-none d-md-block bg-light sidebar">
	          <div class="sidebar-sticky">
	            <ul class="nav flex-column">
	              <li class="nav-item">
	                <a class="nav-link" href="#intro"></i> Intro</a>
	              </li>
	              <li class="nav-item">
	                <a class="nav-link" href="#users"><i class="fas fa-users"></i> Users</a>
	              </li>
	              <li class="nav-item">
	                <a class="nav-link" href="#beers"><i class="fas fa-beer"></i> Beers</a>
	              </li>
	              <li class="nav-item">
	                <a class="nav-link" href="#rating"><i class="fas fa-chart-line"></i> Rating</a>
	              </li>
	            </ul>
	          </div>
        	</nav>
        	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        		<section id="intro">
        			<h1>Intro</h1>
        			<p>At Canpango, we love us some beer! We talk, study, drink and make beer. But we don’t
						have a way to keep track of what everyone’s favorite beer is. We also love RESTful web
						services at Canpango. Here is the API to do exactly that!
					</p>
        		</section>
        		<section id="users" class="pad-top"> 
        			<h1><i class="fas fa-users"></i> Users</h1>
        			<h3>1.1 Create new user (POST method)</h3>
					<p><code>api/beers/user/add</code></p><br/>
					<h4>Parameters</h4>
					<ul>
						<li>username - <i>Minimum of 4 characters required.</i></li>
						<li>password - <i>Minimum of 4 characters required.</i></li>
					</ul>
					<p><b>Example:</b></p>
					<p><code>api/beers/user/add?username=admin200&password=123456</code></p>
        		</section>
        		<section id="beers" class="pad-top"> 
        			<h1><i class="fas fa-beer"></i>Beers</h1>
        			<h3>Create new beers (POST method)</h3>
					<p><code>api/beers/create-new</code></p>
					<h4>Parameters</h4>
					<ul> 
						<li>name</li>
						<li>ibu</li>
						<li>calories</li>
						<li>abv</li>
						<li>style</li>
						<li>brewery_location</li>
						<li>category</li>
					</ul>
					<p><b>Example</b></p>
					<p><code>api/beers/create-new?name=Hunters%20Dry&ibu=30&calories=50&abv=5.20&style=South%20African&brewery_location=South%20Africa,%20Johannesburg&category=9</code></p>

					<h3>Edit beer (PUT method)</h3>

					<p><code>api/beers/update/(:id)</code></p>

					<h4>Parameters</h4>
					<ul>
						<li>id (being updated)</li>
						<li>name</li>
						<li>ibu</li>
						<li>calories</li>
						<li>abv</li>
						<li>style</li>
						<li>brewery_location</li>
						<li>category</li>
					</ul>
					<p><b>Example</b></p>
					<p><code>api/beers/update/1?name=Hunters%Gold&ibu=30&calories=50&abv=5.20&style=South%20African&brewery_location=South%20Africa,%20Johannesburg&category=9</code></p>
					<h3>Delete beer (POST method)</h3>
					<p><code>api/beers/delete/(:num)</code></p>
					<h4>Parameters</h4>
					<ul>
						<li>id (being deleted)</li>
					</ul>
					<p><b>Example:</b></p>
					<p><code>api/beers/delete/1</code></p>
					<h3>View all beers in the system</h3>
					<p><code>api/beers</code></p>
					<p><b>Example:</b></p>
					<p><code>api/beers</code></p>
        		</section>
        		<section id="rating" class="pad-top"> 
        			<h1><i class="fas fa-chart-line"></i> Rating</h1>
        			<h3>Rate each individual beer (POST)</h3>
					<p><code>api/beers/review/(:id)</code></p>
					<h4>Parameters</h4>
					<ul>
						<li>id 	- <i>of the beer being rated, cannot be empty</i></li>
						<li>aroma - <i>rating must be between 1 and 5 and should be numeric.</i></li>
						<li>appearance 	- <i>rating must be between 1 and 5 and should be numeric.</i></li>
						<li>taste - <i>rating must be between 1 and 10 and should be numeric.</i></li>
					</ul>
					<p><b>Example:</b></p>
					<p><code>api/beers/review/2?aroma=4&appearance=4&taste=9</code></p>
					<h3>View a list of ratings for an individual beer (GET)</h3>
					<code>api/beers/review/beer/(:id)</code>
					<h4>Parameters</h4>
					<ul>
						<li>id - <i>of the beer</i></li>
					</ul>
					<p><b>Example:</b></p>
					<code>api/beers/review/beer/1</code>
        		</section>
        	</main>

		</div>
	</div>
<?php $this->load->view('front/includes/footer'); ?>