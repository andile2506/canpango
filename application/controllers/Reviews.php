<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reviews extends CI_Controller {

    public function __construct() { 
        parent::__construct();
        
        //load model
        $this->load->model('Reviews_model');
    }

    public function beer_review($id = 0) {
        
        $id     = $this->uri->segment(5);

        $results   = $this->Reviews_model->getRows($id);
        
        //check if the user data exists
        if(!empty($results)){
            //set the response and exit
            $res = array(
                'status' => TRUE,
                'message' => $results
            );
            echo json_encode($res);
        }else{
            //set the response and exit
            $res = array(
                'status' => FALSE,
                'message' => 'No reviews were found.'
            );
            echo json_encode( $res );
        }
    }
    
    public function review_post() {
        $beerid       = $this->uri->segment(4);
        $aroma        = $this->input->get('aroma');
        $appearance   = $this->input->get('appearance');
        $taste        = $this->input->get('taste');
        $overall      = 0; //initially zero overall
        if(empty($beerid)){ //check if the Beer ID is not empty
            $res = array(
                'status'    => FALSE,
                'message'   => 'Beer ID cannot be empty.'
            );
            echo json_encode($res);
        }else if(!is_numeric($aroma) || ($aroma < 1 || $aroma > 5) ){ //check if rating is numeric or value is between 
            $res = array(
                'status'    => FALSE,
                'message'   => 'Aroma rating must be between 1 and 5 and should be numeric.'
            );
            echo json_encode($res);
        }else if(!is_numeric($appearance) || ($appearance < 1 || $appearance > 5) ){ //check if rating is numeric or value is between 
            $res = array(
                'status'    => FALSE,
                'message'   => 'Appearance rating must be between 1 and 5 and should be numeric.'
            );
            echo json_encode($res);
        }else if(!is_numeric($taste) || ($taste < 1 || $taste > 10) ){ //check if rating is numeric or value is between 
            $res = array(
                'status'    => FALSE,
                'message'   => 'Taste rating must be between 1 and 10 and should be numeric.'
            );
            echo json_encode($res);
        }else{
            $reviewData     = array(); //initialize review data array
            $overallArray   = array($aroma, $appearance, $taste); //array of ratings
            $overallTotal   = array_sum($overallArray); //sum of ratings array

            $reviewData['beerid']       = $beerid;
            $reviewData['aroma']        = $aroma;
            $reviewData['appearance']   = $appearance;
            $reviewData['taste']        = $taste;
            $reviewData['overall']      = $overallTotal;

            $insert = $this->Reviews_model->insert($reviewData, $beerid);    
            //check if the data inserted
            if($insert){
            //set the response and exit
                $res = array(
                    'status'    => TRUE,
                    'message'   => 'Beer review has been added successfully.'
                );
                echo json_encode($res);
            }else{
                //set the response and exit
                $res = array(
                    'status'    => FALSE,
                    'message'   => 'Provide complete all rating'
                );
                echo json_encode($res);
            }
        }
    }
    
    public function beer_put() {
        //get the id to update
        $id = $this->uri->segment(4);
        if(!empty($id)){
            $beerData = array();
            $beerData['name']       = $this->input->get('name');
            $beerData['ibu']        = $this->input->get('ibu');
            $beerData['calories']   = $this->input->get('calories');
            $beerData['abv']        = $this->input->get('abv');
            $beerData['style']      = $this->input->get('style');
            $beerData['brewery_location'] = $this->input->get('brewery_location');
            $beerData['category']   = $this->input->get('category');
            if(!empty($id) && !empty($beerData['name']) && !empty($beerData['ibu']) && !empty($beerData['calories']) && !empty($beerData['abv']) && !empty($beerData['style']) && !empty($beerData['brewery_location']) && !empty($beerData['category']) ){
                //update user data
                $update = $this->Beer_model->update($beerData, $id);  
                //check if the user data updated
                if($update){
                    //set the response and exit
                    $res = array(
                        'status'    => TRUE,
                        'message'   => 'Beer has been updated successfully.'
                    );
                    echo json_encode($res);
                }else{
                    //set the response and exit
                    $res = array(
                        'status'    => FALSE,
                        'message'   => 'Some problems occurred, please try again.'
                    );
                    echo json_encode($res);
                }
            }else{
                //set the response and exit
                $res = array(
                    'status'    => FALSE,
                    'message'   => 'Provide complete beer information to update.'
                );
                echo json_encode($res);
            }
        }else{
            $res = array(
                    'status' => FALSE,
                    'message' => 'ID to update is not defined.'
                );
                echo json_encode($res);
        }
    }
    public function beer_delete($id){
        //check whether post id is not empty
        $id = $this->uri->segment(4);
        if($id){
            //delete post
            $delete = $this->Beer_model->delete($id);
            
            if($delete){
                //set the response and exit
                $res = array(
                    'status' => TRUE,
                    'message' => 'Beer has been removed successfully.'
                );
                echo json_encode($res);
            }else{
                //set the response and exit
                $res = array(
                    'status' => FALSE,
                    'message' => 'Some problems occurred, please try again.'
                );
                echo json_encode($res);
            }
        }else{
            //set the response and exit
            $res = array(
                    'status' => FALSE,
                    'message' => 'No beer were found matching the ID.'
                );
            echo json_encode($res);
        }
    }  
}