<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() { 
        parent::__construct();
        
        //load user model
        $this->load->model('User_model');
    }
    
    public function show_add_user(){
        $data['title'] = 'Add User';
        $this->load->view('front/add_user');
    }

    public function get($id = 0) {
        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned
        $results = $this->User_model->getRows($id);
        
        //check if the user data exists
        if(!empty($results)){
            //set the response and exit
            $res = $results;
            echo json_encode($res);
        }else{
            //set the response and exit
            $res = array(
                    'status' => FALSE,
                    'message' => 'No users were found.'
                );
            echo json_encode( $res );
        }
    }
    
    public function add() {
        //echo "post"; die();
        $username = $this->input->get('username');
        $password = $this->input->get('password');
        if(empty($username) || (strlen($username) < 4) ){
            $res = array(
                'status' => FALSE,
                'message' => 'Username cannot be empty or less than 4 characters.'
            );
            echo json_encode($res);
        }else if(empty($password) || (strlen($password) < 4) ){
            $res = array(
                'status' => FALSE,
                'message' => 'Password cannot be empty or less than 4 characters.'
            );
            echo json_encode($res);
        }else{

            $data = array();
            $data['username'] = $username;
            $data['password'] = password_hash($password, PASSWORD_ARGON2I);
            if(!empty($data['username']) && !empty($data['password'])){
                //insert user data
                $insert = $this->User_model->insert($data);
                //check if the user data inserted
                if($insert){
                    //set the response and exit
                    $res = array(
                        'status' => TRUE,
                        'message' => 'User has been added successfully.'
                    );
                    echo json_encode($res);
                }else{
                    //set the response and exit
                    $res = array(
                        'status' => FALSE,
                        'message' => 'Some problems occurred, please try again'
                    );
                    echo json_encode($res);
                }
            }else{
                //set the response and exit
                $res = array(
                    'status' => FALSE,
                    'message' => 'Provide complete category information to create'
                );
                echo json_encode($res);
            }
        }
    }  
}