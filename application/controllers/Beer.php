<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Beer extends CI_Controller {

    public function __construct() { 
        parent::__construct();
        
        //load user model
        $this->load->model('Beer_model');
    }
    
    public function show_add_beer(){
        $data['title'] = 'Add Beer';
        $this->load->view('front/add_beer');
    }

    public function beer_get($id = 0) {
        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned
        $beer = $this->Beer_model->getRows($id);
        
        //check if the user data exists
        if(!empty($beer)){
            //set the response and exit
            $res = $beer;
            echo json_encode($res);
        }else{
            //set the response and exit
            $res = array(
                    'status' => FALSE,
                    'message' => 'No beers were found.'
                );
            echo json_encode( $res );
        }
    }
    
    //added by form
    public function beer_post() {
        //check last added
        $last_added = $this->Beer_model->date_last_added();
        if($last_added){
            $trimmed_date = substr($last_added, 0, 10); //get the first 10 characters of the date returned
            if($trimmed_date == date('Y-m-d') ){ //add beer only once a day
                $res = array(
                    'status' => FALSE,
                    'message' => 'Beer has already been added today, try again tomorrow.'
                );
                echo json_encode($res);
            }else{
                //adding beer, validate input
                $this->form_validation->set_rules('beername', 'name', 'required');
                $this->form_validation->set_rules('beeribu', 'ibu', 'required');
                $this->form_validation->set_rules('beercalories', 'calories', 'required');
                $this->form_validation->set_rules('beerabv', 'abv', 'required');
                $this->form_validation->set_rules('beerstyle', 'abv', 'required');
                $this->form_validation->set_rules('brewery_location', 'abv', 'required');
                if ($this->form_validation->run() === FALSE){
                    $data['title'] = 'Add Beer';
                    $this->load->view('front/add_beer');
                }else{
                    $beerData = array();
                    $beerData['name']       = $this->input->post('beername');
                    $beerData['ibu']        = $this->input->post('beeribu');
                    $beerData['calories']   = $this->input->post('beercalories');
                    $beerData['abv']        = $this->input->post('beerabv');
                    $beerData['style']      = $this->input->post('beerstyle');
                    $beerData['brewery_location'] = $this->input->post('brewery_location');
                    $beerData['category'] = $this->input->post('category');
                    if(!empty($beerData['name']) && !empty($beerData['ibu']) && !empty($beerData['calories']) && !empty($beerData['abv']) && !empty($beerData['style']) && !empty($beerData['brewery_location']) ){
                        //insert user data
                        $insert = $this->Beer_model->insert($beerData);
                        //check if the user data inserted
                        if($insert){
                            //set the response and exit
                            $res = array(
                                'status' => TRUE,
                                'message' => 'Beer has been added successfully.'
                            );
                            echo json_encode($res);
                        }else{
                            //set the response and exit
                            $res = array(
                                'status' => FALSE,
                                'message' => 'Some problems occurred, please try again'
                            );
                            echo json_encode($res);
                        }
                    }else{
                        //set the response and exit
                        $res = array(
                            'status' => FALSE,
                            'message' => 'Provide complete beer information to create'
                        );
                        echo json_encode($res);
                    }
                }
            }
        }//end last_added
    }

    public function beer_create_post() {
        //check last added
        $last_added = $this->Beer_model->date_last_added();
        if($last_added){
            $trimmed_date = substr($last_added, 0, 10); //get the first 10 characters of the date returned
            if($trimmed_date == date('Y-m-d') ){ //add beer only once a day
                $res = array(
                    'status' => FALSE,
                    'message' => 'Beer has already been added today, try again tomorrow.'
                );
                echo json_encode($res);
            }else{
                $this->get_add_beer();
            }
        }else{//end last_added
            $this->get_add_beer();
        }
    }
    
    function get_add_beer(){
        $beerData = array();
        $beerData['name']       = $this->input->get('name');
        $beerData['ibu']        = $this->input->get('ibu');
        $beerData['calories']   = $this->input->get('calories');
        $beerData['abv']        = $this->input->get('abv');
        $beerData['style']      = $this->input->get('style');
        $beerData['brewery_location'] = $this->input->get('brewery_location');
        $beerData['category'] = $this->input->get('category');
        if(!empty($beerData['name']) && !empty($beerData['ibu']) && !empty($beerData['calories']) && !empty($beerData['abv']) && !empty($beerData['style']) && !empty($beerData['brewery_location']) ){
            //insert user data
            $insert = $this->Beer_model->insert($beerData);
            //check if the user data inserted
            if($insert){
                //set the response and exit
                $res = array(
                    'status' => TRUE,
                    'message' => 'Beer has been added successfully.'
                );
                echo json_encode($res);
            }else{
                //set the response and exit
                $res = array(
                    'status' => FALSE,
                    'message' => 'Some problems occurred, please try again'
                );
                echo json_encode($res);
            }
        }else{
            //set the response and exit
            $res = array(
                'status' => FALSE,
                'message' => 'Provide complete beer information to create'
            );
            echo json_encode($res);
        }
    }
    public function beer_put() {
        //get the id to update
        $id = $this->uri->segment(4);
        if(!empty($id)){
            $beerData = array();
            $beerData['name']       = $this->input->get('name');
            $beerData['ibu']        = $this->input->get('ibu');
            $beerData['calories']   = $this->input->get('calories');
            $beerData['abv']        = $this->input->get('abv');
            $beerData['style']      = $this->input->get('style');
            $beerData['brewery_location'] = $this->input->get('brewery_location');
            $beerData['category']   = $this->input->get('category');
            if(!empty($id) && !empty($beerData['name']) && !empty($beerData['ibu']) && !empty($beerData['calories']) && !empty($beerData['abv']) && !empty($beerData['style']) && !empty($beerData['brewery_location']) && !empty($beerData['category']) ){
                //update user data
                $update = $this->Beer_model->update($beerData, $id);  
                //check if the user data updated
                if($update){
                    //set the response and exit
                    $res = array(
                        'status'    => TRUE,
                        'message'   => 'Beer has been updated successfully.'
                    );
                    echo json_encode($res);
                }else{
                    //set the response and exit
                    $res = array(
                        'status'    => FALSE,
                        'message'   => 'Some problems occurred, please try again.'
                    );
                    echo json_encode($res);
                }
            }else{
                //set the response and exit
                $res = array(
                    'status'    => FALSE,
                    'message'   => 'Provide complete beer information to update.'
                );
                echo json_encode($res);
            }
        }else{
            $res = array(
                    'status' => FALSE,
                    'message' => 'ID to update is not defined.'
                );
                echo json_encode($res);
        }
    }
    public function beer_delete($id){
        //check whether post id is not empty
        $id = $this->uri->segment(4);
        if($id){
            //delete post
            $delete = $this->Beer_model->delete($id);
            
            if($delete){
                //set the response and exit
                $res = array(
                    'status' => TRUE,
                    'message' => 'Beer has been removed successfully.'
                );
                echo json_encode($res);
            }else{
                //set the response and exit
                $res = array(
                    'status' => FALSE,
                    'message' => 'Some problems occurred, please try again.'
                );
                echo json_encode($res);
            }
        }else{
            //set the response and exit
            $res = array(
                    'status' => FALSE,
                    'message' => 'No beer were found matching the ID.'
                );
            echo json_encode($res);
        }
    }  
}