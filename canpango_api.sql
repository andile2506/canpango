-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 23, 2018 at 11:18 PM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `canpango_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `beers`
--

CREATE TABLE `beers` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `ibu` int(10) NOT NULL,
  `calories` int(10) NOT NULL,
  `abv` varchar(10) NOT NULL,
  `style` varchar(50) NOT NULL,
  `brewery_location` varchar(100) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `beers`
--

INSERT INTO `beers` (`id`, `name`, `ibu`, `calories`, `abv`, `style`, `brewery_location`, `created_on`, `category`) VALUES
(1, 'Miller Light', 8, 96, '4.20', 'American Light Lager', 'Milwaukee, Wisconsin', '2018-05-23 06:00:00', '8'),
(2, 'Sayers Stout', 35, 78, '4.50', 'American Light Stout', 'London Brewery', '2018-05-23 06:50:26', '4'),
(3, 'Hunters Dry', 30, 50, '5.20', 'South African', 'South Africa, Johannesburg', '2018-05-23 22:35:00', '9');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) NOT NULL,
  `category` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`) VALUES
(1, 'Pilsner'),
(2, 'Lager'),
(3, 'IPA'),
(4, 'Stout'),
(5, 'Wheat'),
(6, 'Ale'),
(7, 'Light'),
(8, 'Cider');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) NOT NULL,
  `beerid` int(10) NOT NULL,
  `aroma` varchar(10) NOT NULL,
  `appearance` varchar(10) NOT NULL,
  `taste` varchar(10) NOT NULL,
  `overall` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `beerid`, `aroma`, `appearance`, `taste`, `overall`) VALUES
(1, 1, '5', '3', '7', '15'),
(2, 1, '5', '3', '5', '13'),
(3, 1, '5', '3', '4', '12'),
(4, 2, '5', '3', '4', '12');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(160) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `created_on`, `updated_on`) VALUES
(1, 'admin', '$argon2i$v=19$m=1024,t=2,p=2$S2htSTFtSlVXYzh5czR6Zw$vlGPkkipdIsHZpHvwjuRupVT1Ilo4YVK1eRji3phywc', '2018-05-23 21:06:08', '2018-05-23 21:06:08'),
(2, 'admin3', '$argon2i$v=19$m=1024,t=2,p=2$ckpvWDB0T3IuOXdmSGEwRA$Iqqe4sfM/ScIWAweV8iGJieaGmfqdbVBsANnbQ8XhYs', '2018-05-23 21:06:36', '2018-05-23 21:07:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beers`
--
ALTER TABLE `beers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beers`
--
ALTER TABLE `beers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
