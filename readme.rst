#############################################
Welcome to Canpango API made with Codeigniter
#############################################

*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.


************
Installation
************

- Unzip the folder to your local or remote server.
- Import the database canpango_api.sql which is found on the root of the project to your phpmyadmin / workbench.
- Change the base_url in the application/config/config.php file
- Change the database details in the application/config/database.php file